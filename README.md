# m3u8、flv播放器demo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
## 播放器截图
![输入图片说明](public/20230427103429.png)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
